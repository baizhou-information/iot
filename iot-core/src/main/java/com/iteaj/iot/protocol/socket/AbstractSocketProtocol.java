package com.iteaj.iot.protocol.socket;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.BusinessAction;

/**
 * create time: 2021/8/8
 *  用来声明此协议是一个socket协议 如：tcp, udp, smtp等
 * @author iteaj
 * @since 1.0
 */
public abstract class AbstractSocketProtocol<M extends SocketMessage>
        extends AbstractProtocol<M> implements BusinessAction {

    @Override
    public M requestMessage() {
        return super.requestMessage();
    }

    @Override
    public M responseMessage() {
        return super.responseMessage();
    }
}
