package com.iteaj.iot.test.server.api;

import com.iteaj.iot.ProtocolType;

public enum ApiProtocolType implements ProtocolType {
    Stop("stop"),
    Close("close");

    private String desc;

    ApiProtocolType(String desc) {
        this.desc = desc;
    }

    @Override
    public Enum getType() {
        return this;
    }

    @Override
    public String getDesc() {
        return this.desc;
    }

    public static ApiProtocolType getInstance(String value) {
        switch (value) {
            case "Stop": return Stop;
            case "Close": return Close;
        }

        return null;
    }
}
