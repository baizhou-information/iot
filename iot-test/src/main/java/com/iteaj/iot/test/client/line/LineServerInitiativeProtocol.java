package com.iteaj.iot.test.client.line;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.client.protocol.ServerInitiativeProtocol;
import com.iteaj.iot.test.ServerInfoUtil;
import com.iteaj.iot.test.SystemInfo;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.test.message.line.LineMessageBody;
import com.iteaj.iot.test.message.line.LineMessageHead;

public class LineServerInitiativeProtocol extends ServerInitiativeProtocol<LineClientMessage> {

    public LineServerInitiativeProtocol(LineClientMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected void doBuildRequestMessage(LineClientMessage requestMessage) {
        SystemInfo payload = requestMessage.getHead().getPayload();
        ServerInfoUtil.printServerInfo(payload);
    }

    @Override
    protected LineClientMessage doBuildResponseMessage() {
        LineClientMessage clientMessage = requestMessage();
        LineMessageHead head = clientMessage.getHead();
        return new LineClientMessage(LineMessageHead.buildHeader(head.getEquipCode()
                , head.getMessageId(), head.getType(), head.getPayload()), LineMessageBody.build());
    }

    @Override
    public ProtocolType protocolType() {
        return TestProtocolType.PIReq;
    }
}
