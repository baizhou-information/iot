package com.iteaj.iot.boot;

public interface LifeCyclePostProcessor {

    void postProcessBeforeStart();

    void postProcessAfterStart();
}
