package com.iteaj.iot.plc;

import com.iteaj.iot.client.ClientMessage;

public abstract class PlcClientMessage extends ClientMessage {

    public PlcClientMessage(byte[] message) {
        super(message);
    }

    public PlcClientMessage(MessageHead head) {
        super(head);
    }

    public PlcClientMessage(MessageHead head, MessageBody body) {
        super(head, body);
    }
}
