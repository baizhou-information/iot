package com.iteaj.iot.plc.omron;

import com.iteaj.iot.client.protocol.ClientSocketProtocol;
import com.iteaj.iot.consts.ExecStatus;
import com.iteaj.iot.plc.*;
import com.iteaj.iot.plc.ReverseDataTransfer;
import com.iteaj.iot.utils.ByteUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 基于tcp实现的操作欧姆龙PLC设备的协议
 */
public class OmronTcpProtocol extends PlcClientProtocol<OmronMessage> {

    private byte[] data;

    /**
     * 使用默认的客户端
     */
    public OmronTcpProtocol() {
        super(1);
    }

    /**
     * 使用指定配置的客户端
     * @param properties
     */
    public OmronTcpProtocol(OmronConnectProperties properties) {
        super(properties, 1);
    }

    @Override
    public ClientSocketProtocol buildRequestMessage() {
        if(this.requestMessage() != null) {
            return this;
        }

        return super.buildRequestMessage();
    }

    @Override
    protected OmronMessage doBuildRequestMessage() {

        OmronMessageBody requestBody;
        if(this.getWriteAddress() == null) { // 读plc请求
            if(this.getBatchAddress().size() == 1) { // 读取一个地址
                requestBody = OmronMessageBody.buildReadRequestBody(this.getBatchAddress().get(0));
            } else { // 批量读取
                requestBody = OmronMessageBody.buildReadRequestBody(this.getBatchAddress());
            }
        } else { // 写plc请求
            requestBody = OmronMessageBody.buildWriteRequestBody(this.getWriteAddress());
        }

        OmronTcpClient iotClient = (OmronTcpClient) getIotClient();
        OmronConnectProperties config = iotClient.getConfig();
        OmronMessageHeader requestHeader = new OmronMessageHeader()
                .buildRequestHeader(config.getSA1(), config.getDA1(), requestBody.getLength());
        return new OmronMessage(requestHeader, requestBody);
    }

    @Override
    public void doBuildResponseMessage(OmronMessage responseMessage) {
        if(getExecStatus() == ExecStatus.success) {
            OmronMessage requestMessage = requestMessage();
            byte[] message = responseMessage.getMessage();

            try {
                // 读
                if(requestMessage.getMessage()[27] == 0x01) {
                    this.data = OmronUtils.responseValidAnalysis(message, true);
                    this.setCmdStatus(true, "读取成功");
                } else { // 写
                    this.data = OmronUtils.responseValidAnalysis(message, false);
                    this.setCmdStatus(true, "写入成功");
                }
            } catch (PlcException e) {
                this.setCmdStatus(false, e.getMessage());
            }
        }
    }

    @Override
    public DataTransfer getDataTransfer() {
        return OmronDataTransfer.getInstance();
    }

    @Override
    public void writeFull(byte[] fullMessage) {
        this.requestMessage = new OmronMessage(new OmronMessageHeader(fullMessage));
        this.sync(this.getTimeout()).request();
    }

    @Override
    public byte[] readFull(byte[] fullMessage) {
        this.requestMessage = new OmronMessage(new OmronMessageHeader(fullMessage));
        this.sync(this.getTimeout()).request();
        return this.data;
    }

    /**
     * 读取字符串
     * 注：{length} 读取的长度 = length * 2 <br />
     *  比如要读取 "abcde"五个字节的内容那么length = 5 但是实际返回的会是 5 * 2个字节
     * @param address
     * @param length 读取的字节长度 = (length * 2)字节
     * @param encoding 使用的编码
     * @return
     */
    @Override
    public String readString(String address, short length, String encoding) {
        return super.readString(address, length, encoding);
    }

    @Override
    public void write(String address, boolean[] values) {
        if(address == null || values == null) {
            throw new PlcException("参数[address or data]错误");
        }

        byte[] bytes = new byte[values.length];
        for(int i = 0; i < values.length; i++) {
            bytes[i] = values[i] ? (byte) 0x01 : (byte) 0x00;
        }

        this.setWriteAddress(new WriteAddress(bytes, address, AddressType.Bit));

        // 必须使用同步方式读取
        this.sync(this.getTimeout()).request();
    }

    @Override
    protected Class<OmronMessage> getMessageClass() {
        return OmronMessage.class;
    }

    @Override
    public PlcProtocolType protocolType() {
        return PlcProtocolType.Omron;
    }

    @Override
    protected List<byte[]> doRead(List<ReadAddress> batchAddress) {
        return Arrays.asList(this.data);
    }

    @Override
    protected short calcBitLength(short length) {
        return length;
    }

    @Override
    protected boolean[] bytesToBooleans(byte[] bytes, short length) {
        return PLCUtils.byteToBoolArray(bytes, (byte) 0x00);
    }

}
