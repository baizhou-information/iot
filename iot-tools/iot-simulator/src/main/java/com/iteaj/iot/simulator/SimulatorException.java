package com.iteaj.iot.simulator;

import com.iteaj.iot.ProtocolException;

public class SimulatorException extends ProtocolException {

    public SimulatorException(String message) {
        super(message);
    }

    public SimulatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SimulatorException(Throwable cause) {
        super(cause);
    }

    public SimulatorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
