package com.iteaj.iot.taos;

import com.iteaj.iot.handle.proxy.ProtocolHandleProxy;
import com.iteaj.iot.tools.db.DBManager;
import com.iteaj.iot.tools.db.IotMapEntity;

import java.util.List;

public interface TaosSqlManager<P extends ProtocolHandleProxy> extends DBManager<P> {

    /**
     * 解析实体参数到插入sql语句
     * @param tableName
     * @return 返回insert sql
     */
    TaosSqlMeta getDBMeta(String tableName);

    /**
     * 批量数据入库, 针对同一超级表下的数据表
     * @see STable
     * @param entityClazz 必须使用注解{@link STable}
     * @param entities
     * @return
     */
    int batchInsert(Class entityClazz, List<Object> entities);

    /**
     * 批量数据入库, 针对同一超级表下的数据表
     * @see STable
     * @param entityClazz 必须使用注解{@link STable}
     * @param entities
     * @param size 每次入库条数
     * @return
     */
    int batchInsert(Class entityClazz, List<Object> entities, int size);

    /**
     * @param tableName 超级表表名
     * @param entity {@link Object} or {@link IotMapEntity}
     * @return
     */
    @Override
    int insert(String tableName, Object entity);

    /**
     * @see STable
     * @param entityClazz 必须使用注解{@link STable}
     * @param entity {@link Object} or {@link IotMapEntity}
     * @return
     */
    int insert(Class entityClazz, Object entity);

    /**
     * 批量数据入库, 针对同一超级表下的数据表
     * @param tableName 超级表表名
     * @param entities {@link Object} or {@link IotMapEntity}
     * @return
     */
    @Override
    int batchInsert(String tableName, List<Object> entities);

    /**
     * 执行
     * @param value
     * @param handle
     */
    void execute(Object value, P handle);
}
